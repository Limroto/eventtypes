﻿using System;
using System.Collections.Generic;
using System.Linq;
using caalhp.Core.Events;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.IdGenerators;

namespace EventTypes
{
	public class RegisterHelper
	{
		private static RegisterHelper _instance;
		public Dictionary<Type, Dictionary<Type, int>> AffectionTable;

		private RegisterHelper()
		{
			AffectionTable = new Dictionary<Type, Dictionary<Type, int>>();
		}

		/// <summary>
		/// Singleton of the RegisterHelper
		/// </summary>
		public static RegisterHelper Instance
		{
			get { return _instance ?? (_instance = new RegisterHelper()); }
		}

		/// <summary>
		/// Add an affection to the affectionTable
		/// </summary>
		/// <param name="baseTypeEvent">The event that is affected.</param>
		/// <param name="eventAffecting">The event that affects the baseTypeEvent.</param>
		/// <param name="timeAffected">Time in seconds the eventAffecting can affect the baseTypeEvent.</param>
		public void AddAffection(Type baseTypeEvent, Type eventAffecting, int timeAffected)
		{
			var affection = new Dictionary<Type, int>();

			var eventAffectedType = Type.GetType(baseTypeEvent.AssemblyQualifiedName);
			var eventAffectingType = Type.GetType(eventAffecting.AssemblyQualifiedName);

			if (AffectionTable.ContainsKey(eventAffectedType))
			{
				AffectionTable.TryGetValue(eventAffectedType, out affection);
				affection.Add(eventAffectingType, timeAffected);
			}
			else
			{
				affection.Add(eventAffectingType, timeAffected);
				AffectionTable.Add(eventAffectedType, affection);
			}
		}

		/// <summary>
		/// Clear the affection table for all affections.
		/// </summary>
		public void RemoveAffections()
		{
			AffectionTable.Clear();
		}

		/// <summary>
		/// Register a new class with an instance of it.
		/// Needs only be registered ones.
		/// </summary>
		/// <typeparam name="T">A BaseEvent</typeparam>
		/// <param name="theObject">a new istance of an object</param>
		public static void RegisterNewClass<T>(T theObject)
		{
			//All types in hierarchy
			var lvl1Type = theObject.GetType();
			var lvl2Type = theObject.GetType().BaseType;
			var lvl3Type = theObject.GetType().BaseType.BaseType;
			var lvl4Type = theObject.GetType().BaseType.BaseType.BaseType;

			CheckBaseLvl();

			//Check each map for top lvl
			var lvl4Map = CheckLvl4(lvl4Type);

			//Check each map for lvl3
			var lvl3Map = CheckLvl3(lvl3Type, lvl4Map);

			//Check lvl2
			var lvl2Map = CheckLvl2(lvl2Type, lvl4Map, lvl3Map);

			//check lvl1
			CheckLvl1(lvl1Type, lvl4Map, lvl3Map, lvl2Map);
		}

		/// <summary>
		/// Check if the Base level have been registered. If so, return. If not, register.
		/// </summary>
		public static void CheckBaseLvl()
		{
			try
			{
				//Is it allready registered?
				if (BsonClassMap.GetRegisteredClassMaps().Count(x => x.ClassType == new BsonClassMap(typeof(Event)).ClassType) != 0)
					return;

				//Avoid Event is writing to the database
				BsonClassMap.RegisterClassMap<Event>(cm => { });

				BsonClassMap.RegisterClassMap<BaseEvent>(cm =>
				{
					cm.SetIsRootClass(true);
					cm.MapMember(c => c.Data);
					cm.MapMember(c => c.Condition);
					cm.MapMember(c => c.TimeStamp);
					cm.MapIdMember(c => c.Id).SetIdGenerator(GuidGenerator.Instance);
				});
			}
			catch (Exception e)
			{
				Console.WriteLine(e.Message);
			}
		}

		/// <summary>
		///  Checks and registers events at level 1 (DummyBed, CareBed, Shimmer, DummyFall)
		/// </summary>
		/// <param name="lvl1Type">Type for level 1</param>
		/// <param name="lvl4Map">Map for level 4</param>
		/// <param name="lvl3Map">Map for level 3</param>
		/// <param name="lvl2Map">Map for level 2</param>
		/// <returns></returns>
		private static BsonClassMap CheckLvl1(Type lvl1Type, BsonClassMap lvl4Map, BsonClassMap lvl3Map, BsonClassMap lvl2Map)
		{
			var lvl1Found = false;

			var lvl1Map = new BsonClassMap(lvl1Type);
			var maps = BsonClassMap.GetRegisteredClassMaps();
			foreach (var map in maps.Where(map => map.ClassType == lvl1Map.ClassType))
			{
				lvl1Found = true;
				lvl1Map = map;
			}
			//If not found, add discriminator, add to lvl4, lvl3, lvl2 and add to register
			if (!lvl1Found)
			{
				try
				{
					lvl1Map.SetDiscriminator(lvl1Type.Name);
					BsonClassMap.RegisterClassMap(lvl1Map);
					lvl2Map.AddKnownType(lvl1Type);
					lvl3Map.AddKnownType(lvl1Type);
					lvl4Map.AddKnownType(lvl1Type);
				}
				catch (Exception e)
				{
					Console.WriteLine("Level 1 adding went wrong");
					Console.WriteLine(e.Message);
				}
			}
			return lvl1Map;
		}

		/// <summary>
		///  Checks and registers events at level 2 (FallEvent and BedEvent)
		/// </summary>
		/// <param name="lvl2Type">Type of level 2</param>
		/// <param name="lvl4Map">Map of level 3</param>
		/// <param name="lvl3Map">Map of level 4</param>
		/// <returns>ClassMap for level 2</returns>
		private static BsonClassMap CheckLvl2(Type lvl2Type, BsonClassMap lvl4Map, BsonClassMap lvl3Map)
		{
			var lvl2Found = false;

			var lvl2Map = new BsonClassMap(lvl2Type);
			var maps = BsonClassMap.GetRegisteredClassMaps();
			foreach (var map in maps.Where(map => map.ClassType == lvl2Map.ClassType))
			{
				lvl2Found = true;
				lvl2Map = map;
			}
			//If not found, add discriminator, add to lvl4, lvl3 and add to register
			if (!lvl2Found)
			{
				try
				{
					lvl2Map.SetDiscriminator(lvl2Type.Name);
					BsonClassMap.RegisterClassMap(lvl2Map);
					lvl3Map.AddKnownType(lvl2Type);
					lvl4Map.AddKnownType(lvl2Type);
				}
				catch (Exception e)
				{
					Console.WriteLine("Level 2 adding went wrong");
					Console.WriteLine(e.Message);
				}
			}
			return lvl2Map;
		}

		/// <summary>
		///  Checks and registers events at level 3 (Basic and Advanced)
		/// </summary>
		/// <param name="lvl3Type">Type of level 3</param>
		/// <param name="lvl4Map">Map of level 4</param>
		/// <returns>ClassMap for level 3</returns>
		private static BsonClassMap CheckLvl3(Type lvl3Type, BsonClassMap lvl4Map)
		{
			var lvl3Found = false;

			var lvl3Map = new BsonClassMap(lvl3Type);
			var maps = BsonClassMap.GetRegisteredClassMaps();
			foreach (var map in maps.Where(map => map.ClassType == lvl3Map.ClassType))
			{
				lvl3Found = true;
				lvl3Map = map;
			}
			//If not found, add discriminator and add to register
			if (!lvl3Found)
			{
				try
				{
					lvl3Map.SetDiscriminator(lvl3Type.Name);
					BsonClassMap.RegisterClassMap(lvl3Map);
					lvl4Map.AddKnownType(lvl3Type);
				}
				catch (Exception e)
				{
					Console.WriteLine("Level 3 adding went wrong!");
					Console.WriteLine(e.Message);
				}
			}
			return lvl3Map;
		}

		/// <summary>
		/// Checks and registers events at level 4 (top level - BaseEvent).
		/// </summary>
		/// <param name="lvl4Type">The type needed inserted.</param>
		/// <returns>BsonClassMap of the registered input type.</returns>
		private static BsonClassMap CheckLvl4(Type lvl4Type)
		{
			var lvl4Found = false;

			var lvl4Map = new BsonClassMap(lvl4Type);
			var maps = BsonClassMap.GetRegisteredClassMaps();
			foreach (var map in maps.Where(map => map.ClassType == lvl4Map.ClassType))
			{
				lvl4Found = true;
				lvl4Map = map;
			}
			//If not found, add discriminator and add to register
			if (!lvl4Found)
			{
				try
				{
					lvl4Map.SetDiscriminator(lvl4Type.Name);
					BsonClassMap.RegisterClassMap(lvl4Map);
				}
				catch (Exception e)
				{
					Console.WriteLine("Level 4 adding went wrong");
					Console.WriteLine(e.Message);
				}
			}
			return lvl4Map;
		}
	}
}