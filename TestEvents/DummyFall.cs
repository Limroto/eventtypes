﻿using System.Collections.Generic;
using MongoDB.Bson.Serialization.Attributes;

namespace EventTypes.TestEvents
{
	public class DummyFall : FallEvent
	{
		[BsonConstructor]
		public DummyFall()
		{
		}
	}
}