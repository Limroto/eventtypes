﻿using MongoDB.Bson.Serialization.Attributes;

namespace EventTypes.TestEvents
{
	/// <summary>
	/// This is a test class for BedEvent
	/// </summary>
	public class DummyBed : BedEvent
	{
		[BsonConstructor]
		public DummyBed()
		{
		}
	}
}