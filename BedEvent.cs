﻿using MongoDB.Bson.Serialization.Attributes;

namespace EventTypes
{
	/// <summary>
	/// Specific type of Basic event. 
	/// Multiple sensor can be a BedEvent
	/// </summary>
	public abstract class BedEvent : Basic
	{
		[BsonConstructor]
		protected BedEvent()
		{ }
	}
}