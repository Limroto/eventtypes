# EventTypes

This repository is the dependency for the CAALPH EventAnalyser project. 
It will not run without it.

## What is in it?

This repository only contains flat files with events, that can be send from a unit (faked or not) to the rest of the system.

They contain data in a ``List<int>`` and a ``dynamic`` for either a ``true/false`` value or a probability as an ``int``.
