﻿using System.Collections.Generic;
using MongoDB.Bson.Serialization.Attributes;

namespace EventTypes
{
	/// <summary>
	/// Shimmer event. 
	/// Provides a propability of the event and some data for it.
	/// </summary>
	public class Shimmer : FallEvent
	{
		[BsonConstructor]
		public Shimmer()
		{
		}
	}
}