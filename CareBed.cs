﻿using MongoDB.Bson.Serialization.Attributes;

namespace EventTypes
{
	/// <summary>
	/// Specific sensor of the type BedEvent.
	/// This is real data.
	/// </summary>
	public class CareBed : BedEvent
	{
		/// <summary>
		/// This is the concrete implementation for a CareBed sensor.
		/// </summary>
		[BsonConstructor]
		public CareBed()
		{ }
	}
}