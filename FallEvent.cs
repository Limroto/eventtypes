﻿using MongoDB.Bson.Serialization.Attributes;

namespace EventTypes
{
	/// <summary>
	/// FallEvent is sensor registering a person falling.
	/// Raw data and a probability of occourance can be send back
	/// </summary>
	public abstract class FallEvent : Advanced
	{
		[BsonConstructor]
		protected FallEvent()
		{ }
	}
}