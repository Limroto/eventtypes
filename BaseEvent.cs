﻿using System;
using System.Collections.Generic;
using caalhp.Core.Events;
using MongoDB.Bson.Serialization.Attributes;

namespace EventTypes
{
	/// <summary>
	/// All events must comply to this class
	/// The ID and TimeStamp is automatically generated
	/// </summary>
	public abstract class BaseEvent : Event
	{
		public List<int> Data;
		public dynamic Condition;
		public string TimeStamp;

		[BsonId]
		public Guid Id;

		[BsonConstructor]
		protected BaseEvent()
		{
			CallerProcessId = 0;
			Data = new List<int>();
			Condition = 0;
			TimeStamp = DateTime.Now.ToString();
			Id = Guid.NewGuid();
		}
	}

	/// <summary>
	/// All Basic events.
	/// Sensors of this type should not send a probability of the event occouring back
	/// </summary>
	public abstract class Basic : BaseEvent
	{
		[BsonConstructor]
		protected Basic() { }
	}

	/// <summary>
	/// All advanced events
	/// Sensors of this type should send a probability of the event occouring back
	/// </summary>
	public abstract class Advanced : BaseEvent
	{
		[BsonConstructor]
		protected Advanced() { }
	}
}